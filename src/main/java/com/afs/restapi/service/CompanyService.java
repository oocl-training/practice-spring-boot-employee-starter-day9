package com.afs.restapi.service;

import com.afs.restapi.model.Company;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.repository.CompanyJpaRepository;
import com.afs.restapi.repository.EmployeeJpaRepository;
import com.afs.restapi.model.Employee;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CompanyService {


	private final EmployeeJpaRepository employeeJpaRepository;

	private final CompanyJpaRepository companyJpaRepository;


	public List<Company> findAll() {
		return companyJpaRepository.findAll();
	}

	public List<Company> findByPage(Integer page, Integer size) {
		PageRequest pageRequest = PageRequest.of(page - 1, size);
		return companyJpaRepository.findAll(pageRequest).getContent();
	}

	public Company findById(Long id) {
		Company company = companyJpaRepository.findById(id).orElseThrow(CompanyNotFoundException::new);
		List<Employee> employees = employeeJpaRepository.findByCompanyId(company.getId());
		company.setEmployees(employees);
		return company;
	}

	public Company update(Long id, Company company) {
		Company previousCompany = companyJpaRepository.findById(id).orElseThrow(CompanyNotFoundException::new);
		previousCompany.setName(company.getName());
		return companyJpaRepository.save(previousCompany);
	}

	public Company create(Company company) {
		return companyJpaRepository.save(company);
	}

	public List<Employee> findEmployeesByCompanyId(Long id) {
		return employeeJpaRepository.findByCompanyId(id);
	}

	public void delete(Long id) {
		companyJpaRepository.deleteById(id);
	}
}
