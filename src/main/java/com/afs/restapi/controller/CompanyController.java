package com.afs.restapi.controller;

import com.afs.restapi.controller.dto.CompanyRequest;
import com.afs.restapi.controller.dto.CompanyResponse;
import com.afs.restapi.controller.dto.EmployeeResponse;
import com.afs.restapi.controller.mapper.CompanyMapper;
import com.afs.restapi.controller.mapper.EmployeeMapper;
import com.afs.restapi.model.Company;
import com.afs.restapi.service.CompanyService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RequestMapping("companies")
@RestController
public class CompanyController {

	private final CompanyService companyService;

	public CompanyController(CompanyService companyService) {
		this.companyService = companyService;
	}

	@GetMapping
	public List<CompanyResponse> getAllCompanies() {
		return companyService.findAll().stream().map(CompanyMapper::toResponse).collect(Collectors.toList());
	}

	@GetMapping(params = {"page", "size"})
	public List<CompanyResponse> getCompaniesByPage(@RequestParam Integer page, @RequestParam Integer size) {
		return companyService.findByPage(page, size).stream().map(CompanyMapper::toResponse).collect(Collectors.toList());
	}

	@GetMapping("/{id}")
	public CompanyResponse getCompanyById(@PathVariable Long id) {
		return CompanyMapper.toResponse(companyService.findById(id));

	}

	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public CompanyResponse updateCompany(@PathVariable Long id, @RequestBody CompanyRequest companyRequest) {
		Company company = CompanyMapper.toEntity(companyRequest);
		return CompanyMapper.toResponse(companyService.update(id, company));
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteCompany(@PathVariable Long id) {
		companyService.delete(id);
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public CompanyResponse createCompany(@RequestBody CompanyRequest companyRequest) {
		Company company = CompanyMapper.toEntity(companyRequest);
		return CompanyMapper.toResponse(companyService.create(company));
	}

	@GetMapping("/{id}/employees")
	public List<EmployeeResponse> getEmployeesByCompanyId(@PathVariable Long id) {
		return companyService.findEmployeesByCompanyId(id).stream().map(EmployeeMapper::toResponse).collect(Collectors.toList());
	}

}
