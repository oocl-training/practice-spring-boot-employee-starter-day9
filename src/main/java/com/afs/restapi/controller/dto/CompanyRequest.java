package com.afs.restapi.controller.dto;

import lombok.Data;


@Data
public class CompanyRequest {
	private String name;
}
