package com.afs.restapi.controller.dto;


import lombok.Data;

@Data
public class EmployeeResponse {
	private Long id;
	private String name;
	private Integer age;
	private String gender;
	private Long companyId;
}
