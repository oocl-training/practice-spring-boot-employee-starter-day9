package com.afs.restapi.controller.dto;

import lombok.Data;

@Data
public class EmployeeRequest {
	private String name;
	private Integer age;
	private String gender;
	private Integer salary;
	private Long companyId;
}
