package com.afs.restapi.controller.dto;
import lombok.Data;


@Data
public class CompanyResponse {
	private Long id;
	private String name;
	private Integer employeeCount;
}
