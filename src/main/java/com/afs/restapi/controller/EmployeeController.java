package com.afs.restapi.controller;

import com.afs.restapi.controller.dto.EmployeeRequest;
import com.afs.restapi.controller.dto.EmployeeResponse;
import com.afs.restapi.model.Employee;
import com.afs.restapi.controller.mapper.EmployeeMapper;
import com.afs.restapi.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/employees")
@RequiredArgsConstructor
public class EmployeeController {

	private final EmployeeService employeeService;

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public EmployeeResponse createEmployee(@RequestBody EmployeeRequest employeeRequest) {
		Employee employee = EmployeeMapper.toEntity(employeeRequest);
		return EmployeeMapper.toResponse(employeeService.create(employee));
	}

	@GetMapping
	public List<EmployeeResponse> getAllEmployees() {
		return employeeService.findAll().stream().map(EmployeeMapper::toResponse).collect(
				Collectors.toList());
	}

	@GetMapping("/{id}")
	public EmployeeResponse getEmployeeById(@PathVariable Long id) {
		return EmployeeMapper.toResponse(employeeService.findById(id));
	}

	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public EmployeeResponse updateEmployee(@PathVariable Long id, @RequestBody EmployeeRequest employeeRequest) {
		Employee employee = EmployeeMapper.toEntity(employeeRequest);
		return EmployeeMapper.toResponse(employeeService.update(id, employee));
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteEmployee(@PathVariable Long id) {
		employeeService.delete(id);
	}

	@GetMapping(params = "gender")
	public List<EmployeeResponse> getEmployeesByGender(@RequestParam String gender) {
		return employeeService.findAllByGender(gender).stream().map(EmployeeMapper::toResponse).collect(
				Collectors.toList());
	}


	@GetMapping(params = {"page", "size"})
	public List<EmployeeResponse> findEmployeesByPage(@RequestParam Integer page, @RequestParam Integer size) {
		return employeeService.findByPage(page, size).stream().map(EmployeeMapper::toResponse).collect(
				Collectors.toList());
	}

}
