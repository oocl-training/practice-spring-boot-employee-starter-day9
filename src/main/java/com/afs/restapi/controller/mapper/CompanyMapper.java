package com.afs.restapi.controller.mapper;

import com.afs.restapi.controller.dto.CompanyRequest;
import com.afs.restapi.controller.dto.CompanyResponse;
import com.afs.restapi.model.Company;
import org.springframework.beans.BeanUtils;

public class CompanyMapper {
	private CompanyMapper() {

	}

	public static Company toEntity(CompanyRequest request) {
		Company company = new Company();
		BeanUtils.copyProperties(request, company);
		return company;
	}

	public static CompanyResponse toResponse(Company company) {
		CompanyResponse response = new CompanyResponse();
		BeanUtils.copyProperties(company, response);
		if (company.getEmployees() == null) {
			response.setEmployeeCount(0);
		} else {
			response.setEmployeeCount(company.getEmployees().size());
		}
		return response;
	}
}
