package com.afs.restapi.controller.mapper;

import com.afs.restapi.controller.dto.EmployeeRequest;
import com.afs.restapi.controller.dto.EmployeeResponse;
import com.afs.restapi.model.Employee;
import org.springframework.beans.BeanUtils;

public class EmployeeMapper {
	private EmployeeMapper() {

	}

	public static Employee toEntity(EmployeeRequest request) {
		Employee employee = new Employee();
		BeanUtils.copyProperties(request, employee);
		return employee;
	}

	public static EmployeeResponse toResponse(Employee employee) {
		EmployeeResponse response = new EmployeeResponse();
		System.out.println(employee.getCompanyId());
		BeanUtils.copyProperties(employee, response);
		return response;
	}
}
