package com.afs.restapi;

import com.afs.restapi.controller.dto.EmployeeRequest;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeJpaRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
class EmployeeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private EmployeeJpaRepository employeeJpaRepository;

    @BeforeEach
    void setUp() {
        employeeJpaRepository.deleteAll();
    }


    @Test
    void should_create_employee()
    throws Exception {
        // given
        Employee employee = getEmployeeZhangSan();
        EmployeeRequest employeeRequest = new EmployeeRequest();
        BeanUtils.copyProperties(employee, employeeRequest);
        String employeeRequestJson = new ObjectMapper().writeValueAsString(employee);
        // when
        ResultActions response = mockMvc.perform(post("/employees")
                                                         .contentType(MediaType.APPLICATION_JSON)
                                                         .content(employeeRequestJson));
        // then
        response.andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(employee.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(employee.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value(employee.getGender()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").doesNotExist())
                .andExpect(MockMvcResultMatchers.jsonPath("$.companyId").isEmpty());
    }

    @Test
    void should_find_employees()
    throws Exception {
        Employee employee = getEmployeeZhangSan();
        employeeJpaRepository.save(employee);
        mockMvc.perform(get("/employees"))
               .andExpect(MockMvcResultMatchers.status().is(200))
               .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
               .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(employee.getId()))
               .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(employee.getName()))
               .andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(employee.getAge()))
               .andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value(employee.getGender()))
               .andExpect(MockMvcResultMatchers.jsonPath("$[0].salary").doesNotExist())
               .andExpect(MockMvcResultMatchers.jsonPath("$[0].companyId").isEmpty());
    }

    @Test
    void should_find_employee_by_id()
    throws Exception {
        // given
        Employee employee = getEmployeeZhangSan();
        employeeJpaRepository.save(employee);
        // when, then
        mockMvc.perform(get("/employees/{id}", employee.getId()))
               .andExpect(MockMvcResultMatchers.status().is(200))
               .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(employee.getId()))
               .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(employee.getName()))
               .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(employee.getAge()))
               .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value(employee.getGender()))
               .andExpect(MockMvcResultMatchers.jsonPath("$.salary").doesNotExist())
               .andExpect(MockMvcResultMatchers.jsonPath("$.companyId").isEmpty());
    }

    @Test
    void should_find_employee_by_gender()
    throws Exception {
        Employee employee = getEmployeeZhangSan();
        employeeJpaRepository.save(employee);
        mockMvc.perform(get("/employees?gender={0}", "Male"))
               .andExpect(MockMvcResultMatchers.status().is(200))
               .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
               .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(employee.getId()))
               .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(employee.getName()))
               .andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(employee.getAge()))
               .andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value(employee.getGender()))
               .andExpect(MockMvcResultMatchers.jsonPath("$[0].salary").doesNotExist())
               .andExpect(MockMvcResultMatchers.jsonPath("$[0].companyId").isEmpty());
    }

    @Test
    void should_find_employees_by_page()
    throws Exception {
        Employee employeeZhangSan = getEmployeeZhangSan();
        Employee employeeSusan = getEmployeeSusan();
        Employee employeeLisi = getEmployeeLisi();
        employeeJpaRepository.save(employeeZhangSan);
        employeeJpaRepository.save(employeeSusan);
        employeeJpaRepository.save(employeeLisi);

        mockMvc.perform(get("/employees")
                                .param("page", "1")
                                .param("size", "2"))
               .andExpect(MockMvcResultMatchers.status().is(200))
               .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(2))
               .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(employeeZhangSan.getId()))
               .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(employeeZhangSan.getName()))
               .andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(employeeZhangSan.getAge()))
               .andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value(employeeZhangSan.getGender()))
               .andExpect(MockMvcResultMatchers.jsonPath("$[0].salary").doesNotExist())
               .andExpect(MockMvcResultMatchers.jsonPath("$[0].companyId").isEmpty())
               .andExpect(MockMvcResultMatchers.jsonPath("$[1].id").value(employeeSusan.getId()))
               .andExpect(MockMvcResultMatchers.jsonPath("$[1].name").value(employeeSusan.getName()))
               .andExpect(MockMvcResultMatchers.jsonPath("$[1].age").value(employeeSusan.getAge()))
               .andExpect(MockMvcResultMatchers.jsonPath("$[1].gender").value(employeeSusan.getGender()))
               .andExpect(MockMvcResultMatchers.jsonPath("$[1].salary").doesNotExist())
               .andExpect(MockMvcResultMatchers.jsonPath("$[1].companyId").isEmpty());
    }

    @Test
    void should_delete_employee_by_id()
    throws Exception {
        Employee employee = getEmployeeZhangSan();
        employeeJpaRepository.save(employee);

        mockMvc.perform(delete("/employees/{id}", employee.getId()))
               .andExpect(MockMvcResultMatchers.status().is(204));

        assertTrue(employeeJpaRepository.findById(employee.getId()).isEmpty());
    }


    @Test
    void should_update_employee_age_and_salary()
    throws Exception {
        Employee previousEmployee = new Employee(null, "zhangSan", 22, "Male", 1000, null);
        employeeJpaRepository.save(previousEmployee);
        Employee employeeUpdateRequest = new Employee(null, "lisi", 24, "Female", 2000, null);
        ObjectMapper objectMapper = new ObjectMapper();
        String updatedEmployeeJson = objectMapper.writeValueAsString(employeeUpdateRequest);


        mockMvc.perform(put("/employees/{id}", previousEmployee.getId())
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(updatedEmployeeJson))
               .andExpect(MockMvcResultMatchers.status().is(204))
               .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(previousEmployee.getId()))
               .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(previousEmployee.getName()))
               .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(employeeUpdateRequest.getAge()))
               .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value(previousEmployee.getGender()))
               .andExpect(MockMvcResultMatchers.jsonPath("$.salary").doesNotExist())
               .andExpect(MockMvcResultMatchers.jsonPath("$.companyId").isEmpty());
    }


    private static Employee getEmployeeZhangSan() {
        Employee employee = new Employee();
        employee.setName("zhangsan");
        employee.setAge(22);
        employee.setGender("Male");
        employee.setSalary(10000);
        return employee;
    }

    private static Employee getEmployeeSusan() {
        Employee employee = new Employee();
        employee.setName("susan");
        employee.setAge(23);
        employee.setGender("Male");
        employee.setSalary(11000);
        return employee;
    }

    private static Employee getEmployeeLisi() {
        Employee employee = new Employee();
        employee.setName("lisi");
        employee.setAge(24);
        employee.setGender("Female");
        employee.setSalary(12000);
        return employee;
    }
}