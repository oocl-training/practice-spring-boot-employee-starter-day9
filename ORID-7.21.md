## ORID

### O (Objective)

What did we learn today? What activities did you do? What scenes have impressed you?

---

1. Mapper, DTO
2. Microservice
3. Container
4. CI/CD, impress me
5. Cloud Native

---

### R (Reflective)

Please use one word to express your feelings about today's class.

---

abstract

---

### I (Interpretive)

What do you think about this? What was the most meaningful aspect of this activity?

---

1. I used to use Mapper as a Layer to manipulate database, which Mapper mean mapping entity to schema. However, the Mapper I learn today mean mapping entity we saved to entity we transfer, meanwhile introducing the DTO package into the Model package.

2. In the past, I think of microservice just a technique including redis, elasticSearch, MQ and so on, which is a incorrect understanding. It is a architecture to solve the problem of large application or service. 

3. Container represented by docker is a good tools which I used to use during learning Linux. It provide much convenience for us to deploy a application environment.

4. CI/CD also is a convenience method. It concentrate on automatic. I can push and then deploy server. Tool about CI/CD I used to use is github action, with which my blog will update within a few minutes after pushing my new articles.

5. Cloud Native is a series of methods including four elements: devops, ci/cd, microservice, container. 

   Cloud native applications use Docker for Containerization, base on microservices to improve flexibility and maintainability, utilizing DevOps to support CI/CD so that to achieve automation, and use cloud platform facilities to achieve autoscaling and optimizing resource utilization. 

---

### D (Decisional)

Where do you most want to apply what you have learned today? What changes will you make?

---

Cloud Native including its four elements is close to our development. We may have intentionally or unintentionally used some of these tools. After know their think, I will actively learn and use these tools.

---

