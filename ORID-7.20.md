## ORID

### O (Objective)

What did we learn today? What activities did you do? What scenes have impressed you?

---

SQL Basic and Spring Data JPA.
The practice of Spring Data JPA which I never use in the past impress me.

---

### R (Reflective)

Please use one word to express your feelings about today's class.

---

full

---

### I (Interpretive)

What do you think about this? What was the most meaningful aspect of this activity?

---

+ SQL Basic

  include DDL such as create, drop, alter and DML such as select, update, delete, insert.

+ Spring Data JPA

  1. learn JPA history, Spring data JPA is an encapsulation of the Hibernate framework, which is an implementation of the JPA standard. The JPA standard is based on the idea of ORM, which is designed to solve the problem of object persistence.

  2. learn how to add Spring Data JPA library and how to use it.

     Some issues need to be noted, such as 

     that increasing id value won't be reset to 0, 

     that page is count from 0 but we usually count from 1,

     that Spring Data JPA does not cascade queries by default,

     that JPA utilizes Java's reflection mechanism to implement condition of selecting.

---

### D (Decisional)

Where do you most want to apply what you have learned today? What changes will you make?

---

use Spring Data JPA replacing Mybatis

---

